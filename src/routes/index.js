const { Router } = require('express')
const usersRouter = require('./users.router.js')
const cartsRouter = require('./carts.router.js')

const router = Router()

router.use('/api/users', usersRouter)
router.use('/api/carts', cartsRouter)

module.exports = router